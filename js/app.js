"use strict";

/* Revealing module */
var formChecker = (function () {

    // Model
    function Model() {
        this.defaultData = [{"pSKU": "D33H1", "pTitle": "Macbook pro 13 inch", "pPrice": "1500$", "pDesc": "13-inch MacBook Pro with Retina display", "pAvailability": "In-stock"}];
    }

    Model.prototype.getAllProducts = function (query, callback) {
        if(query === "all") {
            var localStorageData = window.localStorage.getItem("form-data");

            if (!localStorageData) {
                localStorageData = this.defaultData;
                window.localStorage.setItem('form-data', JSON.stringify(localStorageData));
            } else {
                localStorageData = JSON.parse(localStorageData);
            }

            callback(localStorageData);
        }
    };

    Model.prototype.saveRowData = function (formData, callback) {
        var resultDataArray = JSON.parse(window.localStorage.getItem("form-data"));

        resultDataArray.push(formData);
        window.localStorage.setItem("form-data", JSON.stringify(resultDataArray));

        callback(formData);
    };


    // View
    function View(tableSelector, formSelector) {
        this.tableSelector = tableSelector;
        this.form = document.querySelector(formSelector);
        this.formElements = this.form.elements;

        this.tableBodySelector = "#products-list";
        this.fieldsPosition = ["pSKU", "pTitle", "pPrice", "pDesc", "pAvailability"];
    }

    View.prototype.render = function (productData) {
        var fieldsPosition = this.fieldsPosition;
        var parentElement;
        var childElement;
        var resultHtml = "";

        if(!Array.isArray(productData)) {
            productData = [productData];
        }

        for (var i = 0; i < productData.length; i++) {
            parentElement = document.createElement("TR");
            for (var j = 0; j < fieldsPosition.length; j++) {
                if (productData[i].hasOwnProperty(fieldsPosition[j])) {
                    fieldsPosition[j] === fieldsPosition[0] ? childElement = document.createElement("TH") : childElement = document.createElement("TD");
                    childElement.innerText = productData[i][fieldsPosition[j]];
                    parentElement.appendChild(childElement);
                }
            }
            resultHtml += parentElement.outerHTML;
        }

        document.querySelector(this.tableSelector + " " + this.tableBodySelector).innerHTML += resultHtml;

        this.form.reset();
    };

    View.prototype.addEventsListeners = function (callback) {
        var self = this;

        /* Onsubmit event for custom handling validation */
        self.form.addEventListener("submit", function (e) {
            e.preventDefault();
            callback(self.formSerialize(self.form));
            return false;
        });

        /* Onselect event for removing error selection */
        var isInputWrapElement;
        var inputWrapElement;

        for (var i = 0; i < this.formElements.length; i++) {
            this.formElements[i].addEventListener("focus", function () {
                isInputWrapElement = true;
                inputWrapElement = this;

                while (isInputWrapElement && (inputWrapElement.nodeName !== "FORM")) {
                    if(inputWrapElement.classList.contains("form-group")) {
                        isInputWrapElement = false;
                        inputWrapElement.classList.remove("has-error");
                    } else {
                        inputWrapElement = inputWrapElement.parentElement;
                    }
                }
            })
        }
    };

    View.prototype.formSerialize = function (form) {
        if (!form || form.nodeName !== "FORM") {
            return false;
        }

        var obj = {};
        var currentElement;

        for (var i = this.formElements.length - 1; i >= 0; i = i - 1) {
            currentElement = this.formElements[i];

            if (currentElement.name === "") {
                continue;
            }
            switch (currentElement.nodeName) {
                case 'INPUT':
                    switch (currentElement.type) {
                        case 'text':
                        case 'hidden':
                        case 'password':
                        case 'button':
                        case 'reset':
                        case 'submit':
                            obj[currentElement.name] = currentElement.value;
                            break;
                        case 'checkbox':
                        case 'radio':
                            currentElement.checked ? obj[currentElement.name] = "In-stock" : obj[currentElement.name] = "Out of stock";
                            break;
                        case 'file':
                            break;
                    }
                    break;
                case 'TEXTAREA':
                    obj[currentElement.name] = currentElement.value;
                    break;
                case 'SELECT':
                    switch (currentElement.type) {
                        case 'select-one':
                            obj[currentElement.name] = currentElement.value;
                            break;
                        case 'select-multiple':
                            for (var j = currentElement.options.length - 1; j >= 0; j = j - 1) {
                                if (currentElement.options[j].selected) {
                                    obj[currentElement.name] = currentElement.options[j].value;
                                }
                            }
                            break;
                    }
                    break;
                case 'BUTTON':
                    switch (currentElement.type) {
                        case 'reset':
                        case 'submit':
                        case 'button':
                            obj[currentElement.name] = currentElement.value;
                            break;
                    }
                    break;
            }
        }

        return obj;
    };

    View.prototype.showInvalidInputs = function (errorFields) {
        for (var i = 0; i < errorFields.length; i++) {
            var isInputWrapElement = true;
            var inputWrapElement = document.getElementById(errorFields[i]);

            while (isInputWrapElement && (inputWrapElement.nodeName !== "FORM")) {
                if (inputWrapElement.classList.contains("form-group")) {
                    isInputWrapElement = false;
                    inputWrapElement.classList.add("has-error")
                } else {
                    inputWrapElement = inputWrapElement.parentElement;
                }
            }
        }
    };

    // Controller
    function Controller(model, view) {
        this.model = model;
        this.view = view;
        this.fieldRules = {pTitle: ["not-empty"], pSKU: ["not-empty"], pPrice: ["not-empty", "float"]};


        // App initialization
        this.renderData();
        this.addEventHandlers();
    }

    Controller.prototype.renderData = function () {
        var self = this;

        self.model.getAllProducts('all', function (data) {
            self.view.render(data);
        });
    };

    Controller.prototype.addEventHandlers = function () {
        this.view.addEventsListeners(this.validateRowData.bind(this));
    };

    Controller.prototype.validateRowData = function (formData) {
        var errorList = [];
        var rules;
        var value;
        var isError;
        var self = this;

        for (var key in this.fieldRules) {
            rules = this.fieldRules[key];
            value = formData[key];
            isError = false;

            for (var i = 0; i < rules.length && !isError; i++) {
                switch (rules[i]) {
                    case "not-empty":
                        if (value.toString().trim().length < 1) {
                            isError = true;
                        }
                        break;

                    case "float":
                        var regex = new RegExp("^\\d+(\\.\\d+)?$");
                        if (!regex.test(value)) {
                            isError = true;
                        }
                        break;
                }
            }

            if (isError) {
                errorList.push(key);
            }
        }

        if (errorList.length > 0) {
            this.view.showInvalidInputs(errorList);
        } else {
            this.model.saveRowData(formData, this.view.render.bind(self.view));
        }
    };

    /* Init application */
    function initApp(formSelector, tableSelector) {
        var controller = new Controller(new Model(), new View(formSelector, tableSelector));
    }
    return {init: initApp};
})();

formChecker.init("#product-table1", "#add-product-form");






